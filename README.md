## README
This tool requires python 3.7.3 or higher. See [Python](https://www.python.org/downloads/)

The shell file **startLocust.sh** runs the tool on host= **aura.dk**

If you wish to run the tool on a different host e.g. **test.aura.dk** then you may open a terminal window in the root of the repository and run the command: **locust --host=test.aura.dk**
Afterwards go to localhost:8089 to start the tool via the ui

# Adding user behavior

To add user behavior simply define a function that simulates the required behavior, in the **locustfile.py**.
An example of a basic function:
```
def myPage(l):
    l.client.get("/my-page/")
```
In the above function the user simply visits a specified page, in this case the page /my-page.
When the function has been defined you may add it to the **tasks** in the **UserBehavior** class along with an integer wich represents the number of times each function should be run.

```
class UserBehavior(TaskSet):
    tasks = {myPage:1,myOtherPage:1}
    def on_start(self):
            homepage(self)
```

For more information on defining user behavior see locusts [documentation](https://docs.locust.io/en/stable/)