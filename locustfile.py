# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 08:54:51 2020

@author: tmo
"""

from locust import HttpLocust, TaskSet
def homepage(l):
    l.client.get("/");
def waooFiber(l):
    l.client.get("/internet-tv-og-telefoni/internet/waoo-fiber/")
def bestilFiber(l):
    l.client.get("/internet-tv-og-telefoni/internet/bestil-fiber/?productId=9357&dawaAddressId=0a3f50c0-31c4-32b8-e044-0003ba298018")
def vaelgElAftale(l):
    l.client.get("/el-og-energi/kob-strom/valg-el-aftale/")
def koebEl(l):
    l.client.get("/el-og-energi/kob-strom/valg-el-aftale/bestilling-aura-fallesel/?productId=20157&wind=True&_ga=2.39285731.1754495509.1581494182-2019327352.1579619369");
def login(l):
    l.client.get("/SelvbetjeningService/ServiceLogin/60007893/vq2wb3/")
def mitAura(l):
    l.client.get("/mit-aura/")

class UserBehavior(TaskSet):
    tasks = {homepage:1,waooFiber:1,vaelgElAftale:1,login:1,mitAura:1}
    def on_start(self):
            homepage(self)

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 1000
    max_wait = 5000